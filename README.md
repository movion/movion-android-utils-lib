Movion Android utils lib
========================

## Instrucciones de uso

### Añadir la siguiente linea configuración en el archivo build.gradle del proyecto
```
allprojects {
    repositories {
        jcenter()
        maven {            
            url "https://api.bitbucket.org/1.0/repositories/movion/maven_repository/raw/releases"
        }
    }
}
```

### Incluir la siguiente dependencia en el archivo de build.gradle de la aplicación:
```
dependencies {
    ...
    compile 'es.movion.utilslib:utils:0.1.10'
    ...
}
```


## [Changelog](CHANGELOG.md)

