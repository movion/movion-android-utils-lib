package es.movion.utilslib;

import android.content.Context;
import android.content.res.AssetManager;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Clase que con utilidaddes sobre el sistema de ficheros
 * 
 * @author rperez
 * @author fvaldivia
 * @author gtirado
 */
public class FileUtils {

	/**
	 * Lee un archivo y lo devuelve en forma de array de bytes
	 * 
	 * @param path ruta de carpeta donde se encuentra el archivo
	 * @param nameFile Nombre del archivo a leer
	 * @return Devuelve el archivo en array de bytes
	 */
	public static byte[] readFile(String path, String nameFile) {
		byte[] data = null;
		File file = new File(path, nameFile);
		InputStream inStream = null;

		try {
			inStream = new FileInputStream(file);
			data = IOUtils.toByteArray(inStream);

		} catch (FileNotFoundException e) {
			AppLog.e("El archivo no existe: path=" + path + ", file=" + nameFile, e);
		} catch (IOException e) {
			AppLog.e("Error al extraer el contenido gzip del archivo: path=" + path + ", file=" + nameFile, e);
		} finally {
			GeneralUtils.closeStream(inStream);
		}

		return data;
	}

	/**
	 * Escribe contenido binario en un fichero
	 * 
	 * @param path ruta del fichero
	 * @param nameFile nombre del fichero
	 * @param rawContent contenido que se quiere escribir
	 * @throws IOException lanza la excepción si no ha podido escribir en el fichero
	 */
	public static void writeInFile(String path, String nameFile, byte[] rawContent) throws IOException {
		File f = new File(path, nameFile);
		FileOutputStream fos = null;

		try {
			f.createNewFile();
			fos = new FileOutputStream(f);
			fos.write(rawContent);
		} finally {
			GeneralUtils.closeStream(fos);
		}
	}

    /**
     * Extrae el contenido de un fichero comprimido en GZIP y lo interta en otro fichero ya descomprimido
     *
     * @param path            ruta del archivo a descomprimir
     * @param nameFile        nombre del fichero a descomprimir
     * @param extractPath     ruta del fichero descomprido
     * @param extractNameFile nombre del fichero descomprimido
     * @throws IOException           lanza la excepción si no ha podido descomprimir correctamente
     * @throws FileNotFoundException lanza la excepción si el fichero no existe
     */
    public static void extractGzipFile(String path, String nameFile, String extractPath, String extractNameFile)
            throws IOException {
        File file = new File(path, nameFile);
        FileInputStream fileInputStream = null;
        InputStream inStream = null;

        try {
            fileInputStream = new FileInputStream(file);
            inStream = new GZIPInputStream(fileInputStream);
            byte[] rawContent = IOUtils.toByteArray(inStream);
            writeInFile(extractPath, extractNameFile, rawContent);
        } finally {
            GeneralUtils.closeStream(inStream);
            GeneralUtils.closeStream(fileInputStream);
        }
    }

	/**
	 * Extrae el contenido de un fichero empaquetado en TAR en una carpeta de destino dado
	 * 
	 * @param path ruta del archivo a descomprimir
	 * @param nameFile nombre del fichero a descomprimir
	 * @param unpackagePath ruta del fichero descomprido
	 * @return devuelve un {@link List} de {@link String} con los nombres de los ficheros desempaquetados
	 * @throws IOException lanza la excepción si ocurre algún error al desempaquetar archivo TAR
	 */
	public static List<String> unpackageTarFile(String path, String nameFile, String unpackagePath) throws IOException {
		List<String> unpackageFiles = new ArrayList<String>();
		File file = new File(path, nameFile);
		InputStream inStream = null;
		TarArchiveInputStream tarInStream = null;

		try {
			// Se abre el archivo para escribir a continuación			
			inStream = new FileInputStream(file);
			tarInStream = new TarArchiveInputStream(inStream);

			TarArchiveEntry tarEntry = tarInStream.getNextTarEntry();

			while (tarEntry != null) {
				if (!tarEntry.isDirectory()) {
					String tarFileName = tarEntry.getName();
					byte[] tarRawContent = new byte[(int) tarEntry.getSize()];
					tarInStream.read(tarRawContent);

					writeInFile(unpackagePath, tarFileName, tarRawContent);
					unpackageFiles.add(tarFileName);
				}

				tarEntry = tarInStream.getNextTarEntry();
			}

		} finally {
			GeneralUtils.closeStream(inStream);
			GeneralUtils.closeStream(tarInStream);
		}

		return unpackageFiles;
	}

	/**
	 * Crea una carpeta, si ya existe no hace nada
	 * 
	 * @param path ruta de la carpeta
	 */
	public static void createFolder(String path) {
		File folder = new File(path);
		if (!folder.exists()) {
			folder.mkdirs();
		}
	}

	/**
	 * Elimina ficheros de una ubicación dada en el sistema de ficheros
	 * 
	 * @param folderPath carpeta en la que queremos borrar los ficheros
	 */
	public static void deleteFilesInFolderPath(String folderPath) {
		deleteFilesInFolderPath(folderPath, null);
	}

	/**
	 * Elimina ficheros de una ubicación dada en el sistema de ficheros, excluye de este borrado la lista de ficheros
	 * que se le pase como parámetro
	 * 
	 * @param folderPath ruta del directorio
	 * @param excludeDeleteFiles lista de ficheros que se quieren excluir del borrado. Son {@link String} que contienen
	 *            exclusivamente el nombre del fichero, es decir, no incluyen su ruta. La comparación del nombre del
	 *            fichero es case sensitive
	 */
	public static void deleteFilesInFolderPath(String folderPath, List<String> excludeDeleteFiles) {
		if(folderPath != null) {
			File folder = new File(folderPath);
			if (folder.exists() && folder.isDirectory()) {
				String[] children = folder.list();

				if (children != null) {
					for (int i = 0; i < children.length; i++) {
						String fileName = (children[i]);

						if (GeneralUtils.isNullOrEmpty(excludeDeleteFiles) || !excludeDeleteFiles.contains(fileName)) {
							File file = new File(folder, children[i]);
							file.delete();
						}
					}
				}
			}
		}
	}

	/**
	 * Copia un archivo desde un origen a un destino
	 *
	 * @param origenPath      directorio de origen
	 * @param origenFileName  archivo de origen
	 * @param destinoPath     directorio de destino
	 * @param destinoFileName archivo de destino
	 */
	public static void moveFile(String origenPath, String origenFileName, String destinoPath, String destinoFileName) throws IOException {
		File source = new File(origenPath, origenFileName);
		File target = new File(destinoPath, destinoFileName);
		if (target.exists()) {
			if (!target.delete()) {
				throw new IOException(" No se ha podido eliminar el fichero " + target.getAbsolutePath() + " y por tanto no se ha podido escribir el nuevo ");
			}
		}

		if (!source.renameTo(target)) {
			throw new IOException(" No se ha podido mover el fichero " + source.getAbsolutePath() + " a " + target.getAbsolutePath());
		}
	}

    /**
     * Copia un fichero desde los assets hacia la ruta de destino que le indiquemos
     *
     * @param context
     * @param assetsFileName
     * @param destPath
     * @param destFileName
     * @return
     * @throws IOException
     */
    public static boolean copyFileFromAssets(Context context, String assetsFileName, String destPath, String destFileName) throws IOException {
        boolean fileCopied = false;
        AssetManager assetManager = context.getAssets();

        File folder = new File(destPath);
        File destFile = new File(destPath, destFileName);
        InputStream in = null;
        try {
            if (!folder.exists())
                folder.mkdirs();
            if (!destFile.exists())
                destFile.createNewFile();

            in = assetManager.open(assetsFileName);
            org.apache.commons.io.FileUtils.copyInputStreamToFile(in, destFile);
            fileCopied = true;
        } finally {
            GeneralUtils.closeStream(in);
        }

        return fileCopied;
    }


    /**
     * Carga un archivo de los assets o del directorio del sandbox dependiendo de dos parámetros,
     * su nombre y de si ha de buscarlo primero en los assets
     *
     * @param context  contexto de la aplicación
     * @param filename nombre del archivo a cargar
     * @return "" o el fichero json como cadena de texto
     */
    public static String loadJSONFileAsString(Context context, String filename) {
        InputStream is = null;
        String json = "";
        try {
            is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            AppLog.e(":: Error en lectura del " + filename + " de que: " + e.getMessage());
        } finally {
            GeneralUtils.closeStream(is);
        }

        return json;
    }

	/**
	 * Carga un archivo de texto
	 *
	 * @param context  contexto de la aplicación
	 * @param filename nombre del archivo a cargar
	 * @return "" o el fichero json como cadena de texto
	 */
	public static String loadFileAsString(Context context, String filename) {
		String fileContent = "";
		try {
			fileContent = loadInputStreamAsString(context.openFileInput(filename));
		} catch (IOException e) {
			AppLog.e("Error en lectura del fichero interno " + filename, e);
		}
		return fileContent;
	}

	/**
	 * Carga un archivo de texto los assets
	 *
	 * @param context  contexto de la aplicación
	 * @param filename nombre del archivo a cargar
	 * @return "" o el fichero json como cadena de texto
	 */
	public static String loadFileAsStringFromAssets(Context context, String filename) {
		String fileContent = "";
		try {
			fileContent = loadInputStreamAsString(context.getAssets().open(filename));
		} catch (IOException e) {
			AppLog.e("Error en lectura del fichero de assets " + filename, e);
		}
		return fileContent;
	}

	/**
	 * Carga un fichero desde
	 *
	 * @param inputStream contexto de la aplicación
	 * @return cadena de texto devuelta
	 */
	private static String loadInputStreamAsString(InputStream inputStream) throws IOException {
		String fileContent = "";
		try {
			StringWriter writer = new StringWriter();
			IOUtils.copy(inputStream, writer, "UTF-8");
			fileContent = writer.toString();
		} finally {
			GeneralUtils.closeStream(inputStream);
		}

		return fileContent;
	}
}
