package es.movion.utilslib;

import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Log de aplicación
 *
 * @author gtirado
 * @author hrahal
 */
public class AppLog {

    public static final int LEVEL_VERBOSE = 1;
    public static final int LEVEL_DEBUG = 2;
    public static final int LEVEL_INFO = 3;
    public static final int LEVEL_WARN = 4;
    public static final int LEVEL_ERROR = 5;
    public static final int LEVEL_WTF = 6;
    public static final int LEVEL_NONE = 7;

    private static final String SEPARATOR = "-";
    private static String appName = "App";
    private static int logLevel = LEVEL_ERROR;

    private static final String FORMAT_DATE = "[dd/MM/yyyy HH:mm:ss]";

    private static final String TAG = "APPLOG";
    private static ExecutorService _logExecutor = Executors.newSingleThreadExecutor();

    private static File fileLog = null;
    private static int fileLogLevel = LEVEL_ERROR;
    private static Object FILE_LOCK = new Object();

    //[start] métodos internos

    private AppLog() {
    }

    /**
     * Indica si para el nivel solicitado se puede imprimir el log o no
     *
     * @param level nivel solicitado
     * @return true si se puede imprimir y false en otro caso
     */
    private static boolean isLoggeable(int level) {
        return level >= getLogLevel();
    }

    /**
     * Devuelve la tag del log con el tag de la aplicación concatenada
     *
     * @param tag tag local
     * @return devuelve el tag de la aplicación
     */
    private static String getAppTag(String tag) {
        return getAppName() + SEPARATOR + tag;
    }

    /**
     * Escribe la línea de log en el fichero de logs
     */
    private static void writeLogToFile(int level, String msg) {
        writeLogToFile(level, getAppName(), msg, null);
    }

    /**
     * Escribe la línea de log en el fichero de logs
     */
    private static void writeLogToFile(int level, String tag, String msg) {
        writeLogToFile(level, getAppTag(tag), msg, null);
    }

    /**
     * Escribe la línea de log en el fichero de logs
     */
    private static void writeLogToFile(int level, String msg, Throwable tr) {
        writeLogToFile(level, getAppName(), msg, tr);
    }

    /**
     * Escribe la línea de log en el fichero de logs
     */
    private static void writeLogToFile(int level, final String tag, final String msg, final Throwable tr) {
        if (level >= fileLogLevel) {
            _logExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    synchronized (FILE_LOCK) {
                        try {
                            // Escribimos si el fichero existe
                            if (existsFileLog()) {
                                String time = DateTools.formatCurrent(FORMAT_DATE);
                                String line = time + " [" + tag + "]: " + msg + '\n' + Log.getStackTraceString(tr)
                                        + '\n';
                                FileUtils.writeStringToFile(fileLog, line, "UTF-8", true);
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "Error writing in log file", e);
                        }
                    }
                }
            });
        }
    }

    //[end] métodos internos

    /**
     * Inicia el volcado de logs en la ruta especifíca con el nivel deseado
     *
     * @param file/
     * @param level nivel que se desea usar para el volcado de logs
     * @return true si se ha creado o abierto (si ya existía) el fichero logs. En este caso el proceso de logs se
     * considera iniciado correctamente. Devuelve false en otro caso
     */
    public static boolean startFileLog(File file, int level) {
        boolean started = false;
        if (file != null && file.exists() && file.isFile()) {
            setFileLogLevel(level);
            fileLog = file;
            started = true;
        } else {
            stopFileLog();
        }

        return started;
    }

    /**
     * Para el proceso de volcado de fichero de logs. Este proceso se encola en la lista de procesos activos, así que se
     * respetarán los procesos anteriores de escritura en fichero. Cuidado!!! La parada obliga a borrar el fichero de
     * logs, así que se borrará cualquier historial.
     */
    public static void stopFileLog() {
        _logExecutor.submit(new Runnable() {
            @Override
            public void run() {
                synchronized (FILE_LOCK) {
                    setFileLogLevel(LEVEL_NONE);
                    try {
                        if (existsFileLog())
                            fileLog.delete();
                    } catch (Exception e) {
                        AppLog.e("APP_LOG", e.getMessage(), e);
                    } finally {
                        fileLog = null;
                    }
                }
            }
        });

    }

    /**
     * Indica si existe actualmente el fichero de logs o no
     *
     * @return true si existe el fichero de logs o false en otro caso
     */
    public static boolean existsFileLog() {
        return fileLog != null && fileLog.exists() && fileLog.isFile();
    }

    public static boolean copyFileLogTo(File destFile) {
        boolean result = false;
        synchronized (FILE_LOCK) {
            if (existsFileLog()) {
                try {
                    FileUtils.copyFile(fileLog, destFile);
                    result = destFile.exists() && destFile.isFile();
                } catch (IOException e) {
                    AppLog.e("APP_LOG", e.getMessage(), e);
                }

            }
        }
        return result;
    }

    /**
     * @return the appName
     */
    public static String getAppName() {
        return appName;
    }

    /**
     * @param appName the appName to set
     */
    public static void setAppName(String appName) {
        AppLog.appName = appName;
    }

    /**
     * @return the logLevel
     */
    public static int getLogLevel() {
        return logLevel;
    }

    /**
     * @param logLevel the logLevel to set
     */
    public static void setLogLevel(int logLevel) {
        AppLog.logLevel = logLevel;
    }

    /**
     * @return the fileLogLevel
     */
    public static int getFileLogLevel() {
        return fileLogLevel;
    }

    /**
     * @param fileLogLevel the fileLogLevel to set
     */
    public static void setFileLogLevel(int fileLogLevel) {
        AppLog.fileLogLevel = fileLogLevel;
    }

	/* Redefinición de metodos de log usando nuestra tag de aplicación */

    // [start] DEBUG

    /**
     * @see Log#d(String, String)
     */
    public static int d(String msg) {
        writeLogToFile(LEVEL_DEBUG, msg);
        return isLoggeable(LEVEL_DEBUG) ? Log.d(getAppName(), msg) : 0;
    }

    /**
     * @see Log#d(String, String)
     */
    public static int d(String tag, String msg) {
        writeLogToFile(LEVEL_DEBUG, tag, msg);
        return isLoggeable(LEVEL_DEBUG) ? Log.d(getAppTag(tag), msg) : 0;
    }

    /**
     * @see Log#d(String, String, Throwable)
     */
    public static int d(String msg, Throwable tr) {
        writeLogToFile(LEVEL_DEBUG, msg, tr);
        return isLoggeable(LEVEL_DEBUG) ? Log.d(getAppName(), msg, tr) : 0;
    }

    /**
     * @see Log#d(String, String, Throwable)
     */
    public static int d(String tag, String msg, Throwable tr) {
        writeLogToFile(LEVEL_DEBUG, tag, msg, tr);
        return isLoggeable(LEVEL_DEBUG) ? Log.d(getAppTag(tag), msg, tr) : 0;
    }

    // [end] DEBUG

    // [start] ERROR

    /**
     * @see Log#e(String, String)
     */
    public static int e(String msg) {
        writeLogToFile(LEVEL_ERROR, msg);
        return isLoggeable(LEVEL_ERROR) ? Log.e(getAppName(), msg) : 0;
    }

    /**
     * @see Log#e(String, String)
     */
    public static int e(String tag, String msg) {
        writeLogToFile(LEVEL_ERROR, tag, msg);
        return isLoggeable(LEVEL_ERROR) ? Log.e(getAppTag(tag), msg) : 0;
    }

    /**
     * @see Log#e(String, String, Throwable)
     */
    public static int e(String msg, Throwable tr) {
        writeLogToFile(LEVEL_ERROR, msg, tr);
        return isLoggeable(LEVEL_ERROR) ? Log.e(getAppName(), msg, tr) : 0;
    }

    /**
     * @see Log#e(String, String, Throwable)
     */
    public static int e(String tag, String msg, Throwable tr) {
        writeLogToFile(LEVEL_ERROR, tag, msg, tr);
        return isLoggeable(LEVEL_ERROR) ? Log.e(getAppTag(tag), msg, tr) : 0;
    }

    // [end] ERROR

    // [start] INFO

    /**
     * @see Log#i(String, String)
     */
    public static int i(String msg) {
        writeLogToFile(LEVEL_INFO, msg);
        return isLoggeable(LEVEL_INFO) ? Log.i(getAppName(), msg) : 0;
    }

    /**
     * @see Log#i(String, String)
     */
    public static int i(String tag, String msg) {
        writeLogToFile(LEVEL_INFO, tag, msg);
        return isLoggeable(LEVEL_INFO) ? Log.i(getAppTag(tag), msg) : 0;
    }

    /**
     * @see Log#i(String, String, Throwable)
     */
    public static int i(String msg, Throwable tr) {
        writeLogToFile(LEVEL_INFO, msg, tr);
        return isLoggeable(LEVEL_INFO) ? Log.i(getAppName(), msg, tr) : 0;
    }

    /**
     * @see Log#i(String, String, Throwable)
     */
    public static int i(String tag, String msg, Throwable tr) {
        writeLogToFile(LEVEL_INFO, tag, msg, tr);
        return isLoggeable(LEVEL_INFO) ? Log.i(getAppTag(tag), msg, tr) : 0;
    }

    // [end] INFO

    // [start] VERBOSE

    /**
     * @see Log#v(String, String)
     */
    public static int v(String msg) {
        writeLogToFile(LEVEL_VERBOSE, msg);
        return isLoggeable(LEVEL_VERBOSE) ? Log.v(getAppName(), msg) : 0;
    }

    /**
     * @see Log#v(String, String)
     */
    public static int v(String tag, String msg) {
        writeLogToFile(LEVEL_VERBOSE, tag, msg);
        return isLoggeable(LEVEL_VERBOSE) ? Log.v(getAppTag(tag), msg) : 0;
    }

    /**
     * @see Log#v(String, String, Throwable)
     */
    public static int v(String msg, Throwable tr) {
        writeLogToFile(LEVEL_VERBOSE, msg, tr);
        return isLoggeable(LEVEL_VERBOSE) ? Log.v(getAppName(), msg, tr) : 0;
    }

    /**
     * @see Log#v(String, String, Throwable)
     */
    public static int v(String tag, String msg, Throwable tr) {
        writeLogToFile(LEVEL_VERBOSE, tag, msg, tr);
        return isLoggeable(LEVEL_VERBOSE) ? Log.v(getAppTag(tag), msg, tr) : 0;
    }

    // [end] VERBOSE

    // [start] WARNING

    /**
     * @see Log#w(String, String)
     */
    public static int w(String msg) {
        writeLogToFile(LEVEL_WARN, msg);
        return isLoggeable(LEVEL_WARN) ? Log.w(getAppName(), msg) : 0;
    }

    /**
     * @see Log#w(String, String)
     */
    public static int w(String tag, String msg) {
        writeLogToFile(LEVEL_WARN, tag, msg);
        return isLoggeable(LEVEL_WARN) ? Log.w(getAppTag(tag), msg) : 0;
    }

    /**
     * @see Log#w(String, String, Throwable)
     */
    public static int w(String msg, Throwable tr) {
        writeLogToFile(LEVEL_WARN, msg, tr);
        return isLoggeable(LEVEL_WARN) ? Log.w(getAppName(), msg, tr) : 0;
    }

    /**
     * @see Log#w(String, String, Throwable)
     */
    public static int w(String tag, String msg, Throwable tr) {
        writeLogToFile(LEVEL_WARN, tag, msg, tr);
        return isLoggeable(LEVEL_WARN) ? Log.w(getAppTag(tag), msg, tr) : 0;
    }

    // [end] WARNING

    // [start] WHAT A TERRIBLE FAILURE

    /**
     * @see Log#wtf(String, String)
     */
    public static int wtf(String msg) {
        writeLogToFile(LEVEL_WTF, msg);
        return isLoggeable(LEVEL_WTF) ? Log.wtf(getAppName(), msg) : 0;
    }

    /**
     * @see Log#wtf(String, String)
     */
    public static int wtf(String tag, String msg) {
        writeLogToFile(LEVEL_WTF, tag, msg);
        return isLoggeable(LEVEL_WTF) ? Log.wtf(getAppTag(tag), msg) : 0;
    }

    /**
     * @see Log#wtf(String, String, Throwable)
     */
    public static int wtf(String msg, Throwable tr) {
        writeLogToFile(LEVEL_WTF, msg, tr);
        return isLoggeable(LEVEL_WTF) ? Log.wtf(getAppName(), msg, tr) : 0;
    }

    /**
     * @see Log#wtf(String, String, Throwable)
     */
    public static int wtf(String tag, String msg, Throwable tr) {
        writeLogToFile(LEVEL_WTF, tag, msg, tr);
        return isLoggeable(LEVEL_WTF) ? Log.wtf(getAppTag(tag), msg, tr) : 0;
    }

    // [end] WHAT A TERRIBLE FAILURE

}