package es.movion.utilslib;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Clase que almacenará utilidades relacionadas con los toast, dialogs, ect
 */
public class ComponentsUtils {


    /**
     * Muestra un toast con un mensaje informativo centrado y de corta duración
     *
     * @param text texto que se mostrará en el toast
     */
    public static void showBasicToast(Context context, String text) {
        showBasicToast(context, text, Toast.LENGTH_SHORT, Gravity.CENTER, 0, 0);
    }

    /**
     * Muestra un toast con un mensaje informativo
     *
     * @param context  contexto de la appliación
     * @param text     texto a mostrar
     * @param duration duración en pantalla
     * @param gravity  posición
     * @param xOffset  desplazamiento x
     * @param yOffset  desplazamiento y
     */
    public static void showBasicToast(Context context, String text, int duration, int gravity, int xOffset, int yOffset) {
        Toast toast = Toast.makeText(context, text, duration);
        toast.setGravity(gravity, xOffset, yOffset);
        toast.show();
    }
}
