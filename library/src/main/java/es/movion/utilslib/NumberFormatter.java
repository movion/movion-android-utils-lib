package es.movion.utilslib;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Formateador de números personalizado
 * 
 * @author hrahal
 *
 */
public class NumberFormatter {
	
	/* Estilos para los formatos de números */
	public final static String DECIMALFORMAT_FLOAT_PATTERN = "#.##0,00";
	public final static String DECIMALFORMAT_INTEGER_PATTERN = "#.###,##";
	public final static char DECIMALFORMAT_GROUP_SEPARATOR = ',';
	public final static char DECIMALFORMAT_DECIMAL_SEPARATOR = '.';

	public static final float DECIMALFORMAT_PERCENTAJE_0 = 0.00f;
	public static final float DECIMALFORMAT_PERCENTAJE_100 = 100.00f;

	private static NumberFormatter _singleton;
	private DecimalFormat _dFormat;
	
	/**
	 * Protegemos al constructor de ser instanciado
	 */
	private NumberFormatter() {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator(DECIMALFORMAT_GROUP_SEPARATOR);
		symbols.setGroupingSeparator(DECIMALFORMAT_DECIMAL_SEPARATOR);
		_dFormat = new DecimalFormat();
		_dFormat.setDecimalFormatSymbols(symbols);
	}
	
	/**
	 * 
	 * @return devuelve una instancia única de esta clase
	 */
	public static NumberFormatter getInstance() {
		if (_singleton == null) {
			_singleton = new NumberFormatter();
		}
		return _singleton;
	}

	/**
	 * Formatea los porcentages de acuerdo con los estilos definidos. Con un valor máximo de 100%
	 *
	 * @param percentage porcentaje que queremos formatear
	 * @return devuelve el porcentaje correctamente formateado
	 */
	public String formatPercentage(float percentage) {
		return formatPercentage(percentage, true);
	}

	/**
	 * Formatea los porcentages de acuerdo con los estilos definidos.
	 *
	 * @param percentage porcentaje que queremos formatear
	 * @return devuelve el porcentaje correctamente formateado
	 */
	public String formatPercentageWithoutMaxValue(float percentage) {
		return formatPercentage(percentage, false);
	}

	/**
	 * Formatea los porcentages de acuerdo con los estilos definidos.
	 * 
	 * @param percentage porcentaje que queremos formatear
	 * @return devuelve el porcentaje correctamente formateado
	 */
	private String formatPercentage(float percentage, boolean withMaxValue) {
		if (withMaxValue && (percentage > DECIMALFORMAT_PERCENTAJE_100))
			percentage = DECIMALFORMAT_PERCENTAJE_100;

		if ((percentage == DECIMALFORMAT_PERCENTAJE_0) || (percentage == DECIMALFORMAT_PERCENTAJE_100)) {
			_dFormat.applyLocalizedPattern(DECIMALFORMAT_INTEGER_PATTERN);
		} else {
			_dFormat.applyLocalizedPattern(DECIMALFORMAT_FLOAT_PATTERN);
		}

		return _dFormat.format(percentage);
	}
	
	/**
	 * Formatea los números de acuerdo con los estilos definidos.
	 * 
	 * @param number número que queremos formatear
	 * @return devuelve el número correctamente formateado
	 */
	public String formatNumber(int number) {
		_dFormat.applyLocalizedPattern(DECIMALFORMAT_INTEGER_PATTERN);
		return _dFormat.format(number);
	}

}
