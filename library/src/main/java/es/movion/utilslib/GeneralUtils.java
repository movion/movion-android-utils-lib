package es.movion.utilslib;

import android.content.Context;
import android.content.res.Configuration;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Clase con utilidades de caracter general
 * 
 * @author rperez
 * @author gtirado
 */
public class GeneralUtils {

	/* Tareas a ejecutar antes de salir de la aplicación */
	private static Runnable exitAppTask;

	/**
	 * Constructor privado para evitar instancias de este objeto
	 */
	private GeneralUtils() {
		// No hacemos nada
	}

	/**
	 * Indica si un array de <code>int</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(int[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>long</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(long[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>float</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(float[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>double</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(double[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>byte</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(byte[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>char</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(char[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>boolean</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(boolean[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>short</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(short[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si un array de <code>Object</code> es nulo o está vacio
	 * 
	 * @param array se trata del array que queremos checkear
	 * @return devuelve true si el array está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(Object[] array) {
		return (array == null || array.length == 0);
	}

	/**
	 * Indica si una {@link Collection} es nula o está vacia
	 * 
	 * @param collection se trata de la colección que queremos checkear
	 * @return devuelve true si la colección está nula o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(Collection collection) {
		return (collection == null || collection.isEmpty());
	}

	/**
	 * Indica si un {@link Map} es nulo o está vacio
	 * 
	 * @param map se trata del mapa que queremos checkear
	 * @return devuelve true si el mapa está nulo o vacio, false en otro caso
	 */
	public static boolean isNullOrEmpty(Map map) {
		return (map == null || map.isEmpty());
	}

	/**
	 * Indica si un <code>String</code> es nulo o está vacio
	 * 
	 * @param str se trata de la cadena que queremos checkear
	 * @return devuelve true si la cadena está nula o vacia, false en otro caso
	 */
	public static boolean isNullOrEmpty(String str) {
		return (str == null || str.length() == 0);
	}

	/**
	 * Muestra un array como cadena de caracteres.
	 * 
	 * @param array array que queremos pasar a String
	 * @return devuelve los primeros len caracteres del array como codena
	 */
	public static String arrayToString(Object array) {
		StringBuffer buffer = new StringBuffer();
		if (array != null) {
			/* Inicializamos el valor lenght */
			int length = 0;

			if (array instanceof int[])
				length = ((int[]) array).length;
			else if (array instanceof long[])
				length = ((long[]) array).length;
			else if (array instanceof float[])
				length = ((float[]) array).length;
			else if (array instanceof double[])
				length = ((double[]) array).length;
			else if (array instanceof byte[])
				length = ((byte[]) array).length;
			else if (array instanceof boolean[])
				length = ((boolean[]) array).length;
			else if (array instanceof short[])
				length = ((short[]) array).length;
			else if (array instanceof Object[])
				length = ((Object[]) array).length;

			buffer.append("[");
			for (int i = 0; i < length; i++) {
				if (i > 0)
					buffer.append(", ");

				if (array instanceof int[])
					buffer.append(((int[]) array)[i]);
				else if (array instanceof long[])
					buffer.append(((long[]) array)[i]);
				else if (array instanceof float[])
					buffer.append(((float[]) array)[i]);
				else if (array instanceof double[])
					buffer.append(((double[]) array)[i]);
				else if (array instanceof byte[])
					buffer.append(((byte[]) array)[i]);
				else if (array instanceof boolean[])
					buffer.append(((boolean[]) array)[i]);
				else if (array instanceof short[])
					buffer.append(((short[]) array)[i]);
				else if (array instanceof Object[])
					buffer.append(((Object[]) array)[i]);
			}
			buffer.append("]");
		} else {
			buffer.append("null");
		}
		return buffer.toString();
	}

	/**
	 * Calcula el valor proporcional de un elemento (regla de tres)
	 * 
	 * <pre>
	 * oldElementReference -> oldElementValue
	 * newElementReference -> x
	 * 
	 * x = (newElementReference * oldElementValue) / oldElementReference
	 * </pre>
	 * 
	 * @param oldElementValue el valor antiguo
	 * @param oldElementReference la referencia antigua (no puede ser cero)
	 * @param newElementReference la nueva referencia
	 * @return calcula el valor proporcional para la nueva referencia a partir de la antigua referencia y su valor
	 */
	public static int getRatioValue(int oldElementValue, int oldElementReference, int newElementReference) {
		int ratioValue = 0;

		if (oldElementReference != 0) {
			ratioValue = (newElementReference * oldElementValue) / oldElementReference;
		}

		return ratioValue;
	}

	/**
	 * Cierra el stream de forma conveniente
	 * 
	 * @param streamToClose
	 */
	public static void closeStream(Closeable streamToClose) {
		try {
			if (streamToClose != null)
				streamToClose.close();

		} catch (IOException e) {
			AppLog.e("Error al cerrar stream", e);
		} finally {
			streamToClose = null;
		}
	}

	/**
	 * Determine if the device is a tablet (i.e. it has a large screen).
	 * 
	 * @param context The calling context.
	 */
	public static boolean isTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;

	}
}
