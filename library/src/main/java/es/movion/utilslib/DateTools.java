package es.movion.utilslib;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Util class to handle date
 *
 * @author hrahal
 */
public class DateTools {

    public static final String DATE_FORMAT_NOW = "HH:mm:ss";
    public static final String DATE_FORMAT_TIME = "HH:mm";
    public static final String DATE_FORMAT_DATE = "dd/MM/yyyy";
    public static final String DATE_FORMAT_FULL = DATE_FORMAT_DATE + " " + DATE_FORMAT_TIME;
    public static final String DATE_FORMAT_ISO_8601 = "yyyy-MM-dd'T'hh:mm:ss";
    public static final String DATE_FORMAT_ISO_8601_TIMEZONE = DATE_FORMAT_ISO_8601 + "Z";
    public static final String DATE_DAY_WEEK_FORMAT = "EEEE";


    private DateTools() {
    }

    /**
     * <p>
     * Gets the current time in milliseconds
     * </p>
     *
     * @return time in milliseconds
     */
    public static long getCurrentTimeInMillis() {
        Calendar cal = Calendar.getInstance();
        return cal.getTimeInMillis();
    }

    /**
     * <p>
     * Get the current time using the selected format. This uses the system timezone.
     * </p>
     *
     * @param format output string format
     * @return formatted string
     */
    public static String formatCurrent(String format) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(cal.getTime());
    }

    /**
     * <p>
     * Formats the date using the selected format and timezone
     * </p>
     *
     * @param date     date to format
     * @param format   output string format
     * @param timezone specified timezone
     * @return formatted string
     */
    public static String format(Date date, String format, TimeZone timezone) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(timezone);
        return sdf.format(date);
    }

    /**
     * <p>
     * Formats the date using the selected format and timezone
     * </p>
     *
     * @param date   date to format
     * @param format output string format
     * @return formatted string
     */
    public static String format(Date date, String format) {
        return format(date, format, TimeZone.getDefault());
    }

    /**
     * <p>
     * Formats the date using the selected format and the GMT timezone
     * </p>
     *
     * @param date   date to format
     * @param format output string format
     * @return formatted string
     */
    public static String formatGMT(Date date, String format) {
        return format(date, format, TimeZone.getTimeZone("GMT"));
    }

    /**
     * <p>
     * Formats the date using the selected format. Uses the system timezone.
     * </p>
     *
     * @param time   in milliseconds
     * @param format output string format
     * @return formatted string
     */
    public static String format(long time, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(time));
    }

    /**
     * <p>
     * Converts the GMT time to the system time zone
     * </p>
     *
     * @param time time in milliseconds (will be interpreted as GMT)
     * @return time in milliseconds using the system time zone
     */
    public static long convertToSystemTimezone(long time) {
        Calendar cal = getCalendarFromLong(time);
        return cal.getTimeInMillis();
    }

    /**
     * <p>
     * Get the value for the specified time
     * </p>
     *
     * @param value can be {@link Calendar.DAY_OF_YEAR}, {@link Calendar.HOUR_OF_DAY} ...
     * @param time  in milliseconds
     * @return current value for the specified time
     */
    public static int getValue(int value, long time) {
        Calendar cal = getCalendarFromLong(time);
        return cal.get(value);
    }

    /**
     * <p>
     * Checks if two dates are on the same day ignoring time.
     * </p>
     *
     * @param time1 the first date in milliseconds
     * @param time2 the second date in milliseconds
     * @return true if they represent the same day
     */
    public static boolean isSameDay(long time1, long time2) {
        Calendar cal1 = getCalendarFromLong(time1);
        Calendar cal2 = getCalendarFromLong(time2);
        return isSameDay(cal1, cal2);
    }

    /**
     * <p>
     * Checks if two dates are on the same day ignoring time.
     * </p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is <code>null</code>
     */
    @NonNull
    public static boolean isSameDay(@NonNull Date date1, @NonNull Date date2) {
        Calendar cal1 = getCalendarFromDate(date1);
        Calendar cal2 = getCalendarFromDate(date2);
        return isSameDay(cal1, cal2);
    }

    /**
     * <p>
     * Checks if two calendars represent the same day ignoring time.
     * </p>
     *
     * @param cal1 the first calendar, not altered, not null
     * @param cal2 the second calendar, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either calendar is <code>null</code>
     */
    public static boolean isSameDay(@NonNull Calendar cal1, @NonNull Calendar cal2) {
        return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * <p>
     * Checks if a date is today.
     * </p>
     *
     * @param time the date in milliseconds
     * @return true if the date is today.
     */
    public static boolean isToday(long time) {
        return isSameDay(time, Calendar.getInstance().getTimeInMillis());
    }

    /**
     * <p>
     * Checks if a date is today.
     * </p>
     *
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    /**
     * <p>
     * Checks if a calendar date is today.
     * </p>
     *
     * @param cal the calendar, not altered, not null
     * @return true if cal date is today
     * @throws IllegalArgumentException if the calendar is <code>null</code>
     */
    public static boolean isToday(Calendar cal) {
        return isSameDay(cal, Calendar.getInstance());
    }

    /**
     * <p>
     * Checks if the first date is before the second date ignoring time.
     * </p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is before the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isBeforeDay(@NonNull Date date1, @NonNull Date date2) {
        Calendar cal1 = getCalendarFromDate(date1);
        Calendar cal2 = getCalendarFromDate(date2);
        return isBeforeDay(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first date is before the second date ignoring time.
     * </p>
     *
     * @param time1 the first date in milliseconds
     * @param time2 the second date in milliseconds
     * @return true if the first date day is before the second date day.
     */
    public static boolean isBeforeDay(long time1, long time2) {
        Calendar cal1 = getCalendarFromLong(time1);
        Calendar cal2 = getCalendarFromLong(time2);
        return isBeforeDay(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first calendar date is before the second calendar date ignoring time.
     * </p>
     *
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is before cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isBeforeDay(@NonNull Calendar cal1, @NonNull Calendar cal2) {

        if (cal1.get(Calendar.ERA) != cal2.get(Calendar.ERA)) {
            return cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA);
        }

        if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)) {
            return cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR);
        }

        return cal1.get(Calendar.DAY_OF_YEAR) < cal2.get(Calendar.DAY_OF_YEAR);


    }

    /**
     * <p>
     * Checks if the first date is after the second date ignoring time.
     * </p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if the first date day is after the second date day.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isAfterDay(@NonNull Date date1, @NonNull Date date2) {
        Calendar cal1 = getCalendarFromDate(date1);
        Calendar cal2 = getCalendarFromDate(date2);
        return isAfterDay(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first date is after the second date ignoring time.
     * </p>
     *
     * @param time1 the first date in milliseconds
     * @param time2 the second date in milliseconds
     * @return true if the first date day is after the second date day.
     */
    public static boolean isAfterDay(long time1, long time2) {
        Calendar cal1 = getCalendarFromLong(time1);
        Calendar cal2 = getCalendarFromLong(time2);
        return isAfterDay(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first calendar date is after the second calendar date ignoring time.
     * </p>
     *
     * @param cal1 the first calendar, not altered, not null.
     * @param cal2 the second calendar, not altered, not null.
     * @return true if cal1 date is after cal2 date ignoring time.
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isAfterDay(@NonNull Calendar cal1, @NonNull Calendar cal2) {
        if (cal1.get(Calendar.ERA) != cal2.get(Calendar.ERA)) {
            return cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA);
        }

        if (cal1.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)) {
            return cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR);
        }

        return cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * <p>
     * Checks if the first date is in the same week of the second date.
     * </p>
     *
     * @param time1 first date in milliseconds
     * @param time2 second date in milliseconds
     * @return true if the dates are in the same week
     */
    public static boolean isSameWeek(long time1, long time2) {
        Calendar cal1 = getCalendarFromLong(time1);
        Calendar cal2 = getCalendarFromLong(time2);
        return isSameWeek(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first date is in the same week of the second date.
     * </p>
     *
     * @param date1 first date
     * @param date2 second date
     * @return true if the dates are in the same week
     * @throws IllegalArgumentException if either of the dates are <code>null</code>
     */
    public static boolean isSameWeek(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return isSameWeek(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first calendar is in the same week of the second calendar.
     * </p>
     *
     * @param cal1 first calendar
     * @param cal2 second calendar
     * @return true if the calendars are in the same week
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isSameWeek(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return cal1.get(Calendar.WEEK_OF_YEAR) == cal2.get(Calendar.WEEK_OF_YEAR);
    }    /**
     * <p>
     * Checks if the first date is in the same month of the second date.
     * </p>
     *
     * @param time1 first date in milliseconds
     * @param time2 second date in milliseconds
     * @return true if the dates are in the same month
     */
    public static boolean isSameMonth(long time1, long time2) {
        Calendar cal1 = getCalendarFromLong(time1);
        Calendar cal2 = getCalendarFromLong(time2);
        return isSameMonth(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first date is in the same month of the second date.
     * </p>
     *
     * @param date1 first date
     * @param date2 second date
     * @return true if the dates are in the same month
     * @throws IllegalArgumentException if either of the dates are <code>null</code>
     */
    public static boolean isSameMonth(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return isSameMonth(cal1, cal2);
    }

    /**
     * <p>
     * Checks if the first calendar is in the same month of the second calendar.
     * </p>
     *
     * @param cal1 first calendar
     * @param cal2 second calendar
     * @return true if the calendars are in the same month
     * @throws IllegalArgumentException if either of the calendars are <code>null</code>
     */
    public static boolean isSameMonth(Calendar cal1, Calendar cal2) {
        if (cal1 == null || cal2 == null) {
            throw new IllegalArgumentException("The dates must not be null");
        }
        return cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }

    /**
     * Get day of week for the date time using the specified format
     *
     * @param time   date in milliseconds
     * @param format format to use
     * @return
     */
    public static String getDayOfWeek(long time, String format) {
        SimpleDateFormat simpleDateformat = new SimpleDateFormat(format);
        return simpleDateformat.format(new Date(time));
    }

    /**
     * Get day of week for the date time using the default format
     *
     * @param time date in milliseconds
     * @return day of week
     */
    public static String getDayOfWeek(long time) {
        return getDayOfWeek(time, DATE_DAY_WEEK_FORMAT);
    }

    /**
     * <p>
     * Checks if the date is yesterday
     * </p>
     *
     * @param time in milliseconds to check
     * @return true if the date is yesterday
     */
    public static boolean isYesterday(long time) {
        Calendar cal = getCalendarFromLong(time);
        return isYesterday(cal);

    }

    /**
     * <p>
     * Checks if the date is yesterday
     * </p>
     *
     * @param date date to check
     * @return true if the date is yesterday
     */
    public static boolean isYesterday(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        Calendar cal1 = getCalendarFromDate(date);
        return isYesterday(cal1);
    }

    /**
     * @param calendar calendar to check
     * @return if the calendar is yesterday
     */
    public static boolean isYesterday(Calendar calendar) {
        Calendar cal2 = Calendar.getInstance();
        cal2.add(Calendar.DAY_OF_YEAR, -1);

        return calendar.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && calendar.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    @NonNull
    private static Calendar getCalendarFromLong(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return calendar;
    }

    @NonNull
    private static Calendar getCalendarFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }
}
