package es.movion.utilslib;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;

/**
 * Utilidades de vista
 */
public class ViewUtils {

    private ViewUtils() {
    }

    public static void hideKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void scrollToRadioGroup(Activity activity, RadioGroup radioGroup) {
        if (radioGroup.getChildCount() > 0) {
            RadioButton lastButton = (RadioButton) radioGroup.getChildAt(radioGroup.getChildCount() - 1);
            hideKeyboard(activity);
            scrollToView(lastButton);
        }
    }

    public static void scrollToView(final View view) {
        final ScrollView scrollView = findScrollView(view);
        if (scrollView != null)
            scrollView.post(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, view.getBottom());
                }
            });
    }

    public static ScrollView findScrollView(View view) {
        return findScrollView(view.getParent());
    }

    private static ScrollView findScrollView(ViewParent view) {
        if (view == null || view instanceof ScrollView)
            return (ScrollView) view;
        else
            return findScrollView(view.getParent());
    }
}
