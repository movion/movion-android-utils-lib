package es.movion.utilslib.text;

/**
 * Filtro que controla el número de decimales permitidos tanto para la parte entera como para la
 * parte decimal de un campo de tipo EditText
 */
public class FloatInputFilter extends RegexInputFilter {

    private static int DEFAULT_VALUE = 2;
    private static String REGEX = "^\\-?(\\d{0,%1$s}|\\d{0,%1$s}\\.\\d{0,%2$s})$";

    /**
     * Constructor por defecto que crea el filtro para que acepta 2 decimales como máximo
     */
    public FloatInputFilter() {
        this(-1, DEFAULT_VALUE);
    }

    /**
     * Construye un filtro para validar campos de tipo float con la posibilidad de fijar
     * un tamaño máximo tanto para la parte entera como para la parte decimal.
     *
     * @param digits   múmero máximo de digitos permitido para la parte entera. -1 si no desea ningún límite
     * @param decimals número máximo de digitos permitido para la parte decimal. -1 si no desea ningún límite
     */
    public FloatInputFilter(int digits, int decimals) {
        super(buildRegex(digits, decimals));
    }

    private static String buildRegex(int digits, int decimals) {
        return String.format(REGEX, digits >= 0 ? digits : "", decimals >= 0 ? decimals : "");
    }
}
