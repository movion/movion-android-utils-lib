package es.movion.utilslib.text;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Filtro para evitar que se inserten caracteres que no cumplan la regex en un edittext
 * NOTA: este filtro debe añadirse al edittext con la regex que se desee
 */
public class RegexInputFilter implements InputFilter {

    private String regex = "";

    public RegexInputFilter(String regex) {
        this.regex = regex;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned destination, int destinationStart, int destinationEnd) {
        // Aplicamos filtro sólo cuando añadimos nuevo texto a la cadena
        // en otro caso quiere decir que estamos eliminando parte de la cadena y lo daremos por válido
        if (end > start) {
            // Construimos el resultado con el texto anterior y la nueva entrada de texto
            String destinationString = destination.toString();
            String resultingTxt = destinationString.substring(0, destinationStart)
                    + source.subSequence(start, end)
                    + destinationString.substring(destinationEnd);
            // Si el texto  coincide con la regex devolvemos null para aceptar el nuevo texto
            // en otro caso devolvemos la cadena vacía que será lo que se pinte
            return resultingTxt.matches(regex) ? null : "";
        }
        return null;
    }
}
