package es.movion.utilslib;

import android.graphics.Color;

public class ColorUtils {
	
	/**
	 * Parsea la cadena pasada por parámetro en un entero que representa un color en hexadecimal
	 * Nota: Se espera una cadena que siga el el formato XXXXXX
	 * 
	 * @param strColor cadena que contiene el color en hexadecimal 
	 * @return representacion entera del color en hexadecimal
	 */
	public static int parseColor(String strColor) {
		int defaultColor = Color.BLACK;
		int color = defaultColor;
		try {
			if (strColor != null && strColor.startsWith("#")) {
				color = Color.parseColor(strColor);
			} else {
				color = strColor != null ? Color.parseColor("#" + strColor) : defaultColor;
			}

		} catch (IllegalArgumentException e) {
			color = defaultColor;
		}
		return color;
	}

	/**
	 * Hace un color más claro usando un factor de escado de 0 a 1
	 * Lightens a color by a given factor.
	 *
	 * @param color color original
	 * @param factor factor para usar. 0 hará que el cólo no cambie y 1 hará el color sea blanco.
	 * @return lighter versión modificada del color
	 */
	public static int lighter(int color, float factor) {
		int red = (int) ((Color.red(color) * (1 - factor) / 255 + factor) * 255);
		int green = (int) ((Color.green(color) * (1 - factor) / 255 + factor) * 255);
		int blue = (int) ((Color.blue(color) * (1 - factor) / 255 + factor) * 255);
		return Color.argb(Color.alpha(color), red, green, blue);
	}

}
