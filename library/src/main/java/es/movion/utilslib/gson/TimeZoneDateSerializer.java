package es.movion.utilslib.gson;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Serializador/Deserializador de fechas con un formato concreto
 */
public class TimeZoneDateSerializer implements JsonSerializer<Date>, JsonDeserializer<Date> {

    private final DateFormat dateFormat;

    /**
     * Constructor. Zona horaria GMT
     *
     * @param dateFormat
     */
    public TimeZoneDateSerializer(String dateFormat) {
        this(dateFormat, "GMT");
    }

    /**
     * Constructor
     *
     * @param dateFormat
     * @param timeZone
     */
    public TimeZoneDateSerializer(String dateFormat, String timeZone) {
        this.dateFormat = new SimpleDateFormat(dateFormat);
        this.dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
    }

    @Override
    public synchronized JsonElement serialize(Date date, Type type,
                                              JsonSerializationContext jsonSerializationContext) {
        synchronized (dateFormat) {
            String dateFormatAsString = dateFormat.format(date);
            return new JsonPrimitive(dateFormatAsString);
        }
    }

    @Override
    public synchronized Date deserialize(JsonElement jsonElement, Type type,
                                         JsonDeserializationContext jsonDeserializationContext) {
        try {
            synchronized (dateFormat) {
                return dateFormat.parse(jsonElement.getAsString());
            }
        } catch (ParseException e) {
            return null;
        }
    }
}
