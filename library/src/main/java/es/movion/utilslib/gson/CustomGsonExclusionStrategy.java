package es.movion.utilslib.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * Estrategia para excluir aquellas clases y propiedades de una clase de la serializacion/deserializacion de Gson.
 *
 * Para ello habrá que marcar dichas clases con la anotación {@link ExcludeGson}
 */
public class CustomGsonExclusionStrategy implements ExclusionStrategy {

    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return clazz.getAnnotation(ExcludeGson.class) != null;
    }

    @Override
    public boolean shouldSkipField(FieldAttributes f) {
        return f.getAnnotation(ExcludeGson.class) != null;
    }
}
