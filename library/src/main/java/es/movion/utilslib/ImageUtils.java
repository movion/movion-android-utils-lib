package es.movion.utilslib;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Clase de utilidades para manejo de imagenes
 * 
 * @author hrahal
 *
 */
public class ImageUtils {

	@TargetApi(VERSION_CODES.KITKAT)
	public static int getBitmapSize(Bitmap bitmap) {
		// From KitKat onward use getAllocationByteCount() as allocated bytes can potentially be
		// larger than bitmap byte count.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			return bitmap.getAllocationByteCount();
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
			return bitmap.getByteCount();
		}

		// Pre HC-MR1
		return bitmap.getRowBytes() * bitmap.getHeight();
	}

	/**
	 * A partir del nombre del icono recuperamos el identificador del recurso
	 *
	 * @param context contexto
	 * @param resName nombre del recurso
	 * @return identificador del recurso
	 */
	public static int getImageResource(Context context, String resName) {
		return context.getResources().getIdentifier(resName, "drawable", context.getPackageName());
	}

	/**
	 * Método que devuelve la path completa de la imagen dada por una URI
	 *
	 * @param context contexto de la aplicación
	 * @param contentUri Uri de la que extraer la ruta d ela imagen
	 * @return
	 */
	public static String getRealPathFromURI(Context context, Uri contentUri) {
		String pathOut = null;
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);

		if (loader != null) {
			Cursor cursor = loader.loadInBackground();
			if (cursor != null) {
				int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				pathOut = cursor.getString(column_index);
			}
		}

		return pathOut;
	}

	/**
	 * Devuelve el nombre del fichero a partir de la URI de este
	 *
	 * @param context contexto de la aplicación
	 * @param uri uri del fichero
	 * @return filename nomble del fichero recuperado
	 */
	public static String getNameFile(Context context, Uri uri) {
		File file = new File(ImageUtils.getRealPathFromURI(context, uri));
		return file != null ? file.getName() : "";
	}

	/**
	 * Get Uri from File Path
	 *
	 * @param context
	 * @param imageFile
	 * @return
	 */
	public static Uri getImageContentUri(Context context, File imageFile) {
		String filePath = imageFile.getAbsolutePath();
		Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				new String[] { MediaStore.Images.Media._ID }, MediaStore.Images.Media.DATA + "=? ",
				new String[] { filePath }, null);
		if (cursor != null && cursor.moveToFirst()) {
			int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
			Uri baseUri = Uri.parse("content://media/external/images/media");
			return Uri.withAppendedPath(baseUri, "" + id);
		} else {
			if (imageFile.exists()) {
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.DATA, filePath);
				return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			} else {
				return null;
			}
		}
	}

	/**
	 * Método que recupera una imagen de recursos y recude su calidad para ocupar menos en memoria
	 *
	 * @param res acceso a la carpeta de recursos de la aplicación
	 * @param resId identificador de la imagen a buscar en recursos
	 * @param reqWidth ancho resultante deseado (se mantiene el aspect ratio)
	 * @param reqHeight alto resultante deseado (se mantiene el aspect ratio)
	 * @return imagen reducida
	 */
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		options.inPurgeable = true;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public static Bitmap decodeSampledBitmapFromFile(String pathName, int reqWidth, int reqHeight) {
		return decodeSampledBitmapFromFile(pathName, reqWidth, reqHeight, false, false);
	}

	/**
	 * Método que recupera una imagen desde fichero y reduce su calidad para ocupar menos en memoria
	 *
	 * @param pathName ruta a la imagen original
	 * @param reqWidth ancho resultante deseado (se mantiene el aspect ratio)
	 * @param reqHeight alto resultante deseado (se mantiene el aspect ratio)
	 * @param scaleToFit si es true escalamos hasta conseguir el mejor ajuste a la dimesiones requeridas en otro caso no se hace nada
	 * @param checkExif si es true leemos la información EXIF contenida en la fichero original y la usamos para rotar la imagen si es necesario en otro caso no se hace nada
	 *
	 * @return imagen reducida
	 */
	public static Bitmap decodeSampledBitmapFromFile(String pathName, int reqWidth, int reqHeight, boolean scaleToFit, boolean checkExif) {
		// Primero decodificamos usando inJustDecodeBounds=true para revisar las dimensiones originales
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(pathName, options);
		AppLog.d("Original dimens: " + options.outWidth + "x" + options.outHeight);
		// Calculamos inSampleSize para el ancho y alto deseados
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decodificamos el bitmap con el inSampleSize calculado
		options.inJustDecodeBounds = false;
		Bitmap bmp = BitmapFactory.decodeFile(pathName, options);
		// Bitmap resultado
		Bitmap resBmp = null;
		if (bmp != null) {
			// rotamos la imagen si se solicita una revisión de su contenido EXIF
			Bitmap rotatedBmp = null;
			if (checkExif) {
				ExifInterface exif = null;
				try {
					exif = new ExifInterface(pathName);
				} catch (IOException e) {
					AppLog.e("Error reading exif from file", e);
					exif = null;
				}
				// Si el EXIF existe leeemos la información y rotamos según la orientación
				if (exif != null) {
					int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
					rotatedBmp = rotateBitmap(bmp, orientation);
				} else {
					// en otro caso dejamos el bitmap original
					rotatedBmp = bmp;
				}
			} else {
				// en otro caso dejamos el bitmap original
				rotatedBmp = bmp;
			}

			// Escalamos para ajustar a las dimensiones si es necesario
			if (scaleToFit && rotatedBmp != null) {
				// calculamos el ratio en lugar de usar el factor inSample anterior
				int resWidth = rotatedBmp != null ? rotatedBmp.getWidth() : 0;
				int resHeight = rotatedBmp != null ? rotatedBmp.getHeight() : 0;
				float ratioWidth = 1;
				float ratioHeight = 1;
				if (reqWidth > 0) {
					ratioWidth = (float) resWidth / (float) reqWidth;
				}
				if (reqHeight > 0) {
					ratioHeight = (float) resHeight / (float) reqHeight;
				}
				float ratio = Math.max(ratioWidth, ratioHeight);
				resWidth = Math.round((float) resWidth / ratio);
				resHeight = Math.round((float) resHeight / ratio);
				resBmp = Bitmap.createScaledBitmap(rotatedBmp, resWidth, resHeight, true);
				if (resBmp != rotatedBmp) {
					// liberamos el bitmap antiguo si uno nuevo fue generado
					rotatedBmp.recycle();
					rotatedBmp = null;
				}
			} else {
				resBmp = rotatedBmp;
			}
		}

		if (resBmp != null)
			AppLog.d("Result dimens: " + resBmp.getWidth() + "x" + resBmp.getHeight());
		return resBmp;
	}

	/**
	 * Rota un bitmap según la orientación requerida
	 *
	 * @param bitmap bitmap a rotar
	 * @param orientation dirección hacia la que rotar
	 *
	 * @return bitmap rotado o el bitmap original si no fue precisada la rotación
	 */
	public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
		Bitmap rotatedBmp = null;
		if (bitmap != null) {
			try {
				Matrix matrix = new Matrix();
				switch (orientation) {
					case ExifInterface.ORIENTATION_NORMAL:
						matrix = null;
						break;
					case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
						matrix.setScale(-1, 1);
						break;
					case ExifInterface.ORIENTATION_ROTATE_180:
						matrix.setRotate(180);
						break;
					case ExifInterface.ORIENTATION_FLIP_VERTICAL:
						matrix.setRotate(180);
						matrix.postScale(-1, 1);
						break;
					case ExifInterface.ORIENTATION_TRANSPOSE:
						matrix.setRotate(90);
						matrix.postScale(-1, 1);
						break;
					case ExifInterface.ORIENTATION_ROTATE_90:
						matrix.setRotate(90);
						break;
					case ExifInterface.ORIENTATION_TRANSVERSE:
						matrix.setRotate(-90);
						matrix.postScale(-1, 1);
						break;
					case ExifInterface.ORIENTATION_ROTATE_270:
						matrix.setRotate(-90);
						break;
					default:
						matrix = null;
						break;
				}

				if (matrix != null) {
					rotatedBmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
					if (rotatedBmp != bitmap) {
						// liberamos el bitmap antiguo si uno nuevo fue generado
						bitmap.recycle();
						bitmap = null;
					}
				} else {
					rotatedBmp = bitmap;
				}
			} catch (Exception e) {
				AppLog.e("An error occurred while trying to rotate bitmap", e);
				rotatedBmp = bitmap;
			}
		}

		return rotatedBmp;
	}

	/**
	 * Calcula el ratio para re-escalar a las dimensiones requeridas usando la información leída de un bitmap
	 *
	 * @param options opciones del bitmap (contiene la información del ancho y alto originales)
	 * @param reqWidth ancho requerido
	 * @param reqHeight alto requerido
	 *
	 * @return ratio calculado (este ratio siempre es potencia de 2) si el ratio es 1 no es necesario re-escalar
	 */
	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Recuperamos el tamaño original de la imagen
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		// Comprobamos si es necesario un re-escalado
		if (reqWidth > 0 || reqHeight > 0) {
			if (height > reqHeight || width > reqWidth) {
				// Si uno de los parámetros es <=0 calcula su tamaño proporcionado
				if ((reqWidth <= 0) && (reqHeight > 0)) {
					reqWidth = getImageRatioWidth(width, height, reqHeight);
				} else if ((reqHeight <= 0) && (reqWidth > 0)) {
					reqHeight = getImageRatioHeight(width, height, reqWidth);
				}

				// Calculamos el ratio (en potencia de 2) para que ambas dimensiones queden por debajo del original
				while ((height / inSampleSize) > reqHeight && (width / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
			}
		}

		return inSampleSize;
	}

	/**
	 * Calculate a new image ratio height in base of the old width and height and the new width
	 *
	 * @param oldWidth the old width of the image
	 * @param oldHeight the old height of the image
	 * @param newWidth the new width of the image
	 * @return return the new image ratio height in base of the old width and height and the new width
	 */
	public static int getImageRatioHeight(int oldWidth, int oldHeight, int newWidth) {
		return GeneralUtils.getRatioValue(oldHeight, oldWidth, newWidth);
	}

	/**
	 * Calculate a new image ratio width in base of the old width and height and the new height
	 *
	 * @param oldWidth the old width of the image
	 * @param oldHeight the old height of the image
	 * @param newWidth the new width of the image
	 * @return return the new image ratio height in base of the old width and height and the new width
	 */
	public static int getImageRatioWidth(int oldWidth, int oldHeight, int newWidth) {
		return GeneralUtils.getRatioValue(oldWidth, oldHeight, newWidth);
	}

	public static Bitmap getImageFromBase64(String codedImage) {
		if (codedImage != null) {
			byte[] decodedString = Base64.decode(codedImage, Base64.DEFAULT);
			Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
			return decodedByte;
		} else {
			return null;
		}
	}

	public static String getBase64FromImage(Bitmap bitmap) {
		return getBase64FromImage(bitmap, Bitmap.CompressFormat.PNG, 100);
	}

	public static String getBase64FromImage(Bitmap bitmap, Bitmap.CompressFormat compressFormat, int quality) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		bitmap.compress(compressFormat, quality, outputStream);
		String base64Str = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
		GeneralUtils.closeStream(outputStream);
		return base64Str;
	}
}
